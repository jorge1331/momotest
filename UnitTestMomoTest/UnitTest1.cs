﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using momoTest.Controllers;
using momoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UnitTestMomoTest
{
    [TestClass]
    public class UnitTest1
    {
        ValuesController vc = new ValuesController();
        momoTestDataContextDataContext momoContext = new momoTestDataContextDataContext();

        [TestMethod]
        public void GetAllCustomersTest()
        {
            //Arrange
            int i = 0;
            List<CustomerModel> expectedCustomer =  momoContext.Customers.Select(c => new CustomerModel()
            {
                ContactName = c.ContactName,
                CompanyName = c.CompanyName,
                PhoneNumber = c.PhoneNumber
            }).ToList();

            //Act
            List<CustomerModel> actualCustomer = vc.GetAllCustomers();

            //Assert
            foreach (var c in actualCustomer)
            {

                Assert.AreEqual(c.CompanyName, expectedCustomer[i].CompanyName);
                Assert.AreEqual(c.ContactName, expectedCustomer[i].ContactName);
                Assert.AreEqual(c.TelephoneNumber, expectedCustomer[i].TelephoneNumber);
                i++;
            }
        }


        [TestMethod]
        public void GetCustomerByIdTest()
        {
            //Arrange
            int Id = 1;
            Customer expectedCustomer = new Customer()
            {
                Address = "63 Av. Sur Colonia y Pasaje Santa Mónica No.19, San Salvador.",
                CompanyName = "MOMO Centro America",
                ContactName = "Silvia Hernandez",
                Country = "El Salvador",
                CustomerID = 1,
                PhoneNumber = "2245-1700"
            };

            //Act
            CustomerModel actualCustomer = vc.GetCustomerById(Id);

            //Assert
            Assert.AreEqual(expectedCustomer.CustomerID, actualCustomer.CustomerId);
            Assert.AreEqual(expectedCustomer.Address, actualCustomer.Address);
            Assert.AreEqual(expectedCustomer.CompanyName, actualCustomer.CompanyName);
            Assert.AreEqual(expectedCustomer.ContactName, actualCustomer.ContactName);
            Assert.AreEqual(expectedCustomer.Country, actualCustomer.Country);
            Assert.AreEqual(expectedCustomer.PhoneNumber, actualCustomer.TelephoneNumber);
        }


        [TestMethod]
        public void GetCustomerOrderHistoryTest()
        {
            //Arrange
            int Id = 1;
            int i = 0;
            List<OrderModel> expectedOrders = momoContext.Orders.Where(c => c.CustomerID == Id).Select(o => new OrderModel()
            {
                Amount = o.Amount,
                Description = o.Description
            }).ToList();

            //Act
            List<OrderModel> actualOrders = vc.GetCustomerOrderHistory(Id).ToList();

            //Assert
            foreach (var o in actualOrders)
            {
                Assert.AreEqual(o.Amount, expectedOrders[i].Amount);
                Assert.AreEqual(o.Description, expectedOrders[i].Description);
                i++;
            }
        }

        [TestMethod]
        public void GetCustomerOrdersByIdTest()
        {
            //Arrange
            int Id = 1;
            int IdOrder = 1;
            Order expectedOrder = new Order()
            {
                OrderID = Id,
                Description = "Order 333",
                Date = DateTime.Parse("2017-09-09 00:00:00.000"),
                Amount = 133.2,
                Quantity = 1,
                CustomerID = 3
            };

            //Act
            OrderModel actualOrder = vc.GetCustomerOrdersById(Id, IdOrder);

            //Assert
            Assert.AreEqual(actualOrder.OrderId, actualOrder.OrderId);
            Assert.AreEqual(actualOrder.Description, actualOrder.Description);
            Assert.AreEqual(actualOrder.Date, actualOrder.Date);
            Assert.AreEqual(actualOrder.Amount, actualOrder.Amount);
            Assert.AreEqual(actualOrder.Quantity, actualOrder.Quantity);
            Assert.AreEqual(actualOrder.CustomerId, actualOrder.CustomerId);

        }

        [TestMethod]
        public void sumaTest()
        {
            //Arrange
            int num1 = 2;
            int num2 = 2;
            int esperado = 4;

            //Act
            int actual = vc.suma(num1, num2);

            //Assert
            Assert.AreEqual(esperado, actual);

        }


    }
}
