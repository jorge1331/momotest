Create database momoTest;

USE [momoTest]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 07/09/2017 18:11:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](200) NULL,
	[Country] [varchar](100) NULL,
	[Address] [varchar](200) NULL,
	[ContactName] [varchar](100) NULL,
	[PhoneNumber] [varchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 07/09/2017 18:11:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](500) NULL,
	[Date] [datetime] NULL,
	[Amount] [float] NULL,
	[Quantity] [int] NULL,
	[CustomerID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (1, N'MOMO Centro America', N'El Salvador', N'63 Av. Sur Colonia y Pasaje Santa Mónica No.19, San Salvador.', N'Silvia Hernandez', N' 2245-1700')
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (2, N'Ransa Operador Logistico', N'El Salvador', N'Redondel integracion.', N'Ricardo Palma', N' 2513-2300')
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (3, N'Creativa Consultores', N'El Salvador', N'Col. San Francisco', N'Jorge Marquez', N' 2532-7000')
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (4, N'El diario de hoy', N'El Salvador', N'San salvador', N'Napoleón Altamirano', N' 2532-0000')
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (5, N'La prensa grafica', N'El Salvador', N'San salvador', N'Jose Dutriz', N' 2522-0000')
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (6, N'Company Test', N'Honduras', N'Tegucigalpa', N'Jorge Mendez', N'73535353')
INSERT [dbo].[Customer] ([CustomerID], [CompanyName], [Country], [Address], [ContactName], [PhoneNumber]) VALUES (7, N'Company Test', N'Honduras', N'Tegucigalpa', N'Jorge Mendez', N'73535353')
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (1, N'Oder 1', CAST(0x0000A7E60179190D AS DateTime), 10, 2, 1)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (2, N'Oder 2', CAST(0x0000A7E60179B044 AS DateTime), 20, 4, 1)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (3, N'Oder 3', CAST(0x0000A7E60181B48F AS DateTime), 30, 6, 2)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (4, N'Oder 4', CAST(0x0000A7E60181B4A2 AS DateTime), 40, 8, 2)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (5, N'Oder 5', CAST(0x0000A7E60181B4A3 AS DateTime), 50, 10, 3)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (6, N'Oder 6', CAST(0x0000A7E60181B4A3 AS DateTime), 60, 12, 3)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (7, N'Oder 7', CAST(0x0000A7E60181B4A4 AS DateTime), 70, 14, 4)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (8, N'Oder 8', CAST(0x0000A7E60181B4A4 AS DateTime), 80, 16, 4)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (9, N'Oder 9', CAST(0x0000A7E60181B4A4 AS DateTime), 100, 18, 5)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (10, N'Oder 10', CAST(0x0000A7E60181B4A4 AS DateTime), 120, 20, 5)
INSERT [dbo].[Orders] ([OrderID], [Description], [Date], [Amount], [Quantity], [CustomerID]) VALUES (11, N'ddd', CAST(0x0000A7E700EBFD0B AS DateTime), 32, 32, 2)
SET IDENTITY_INSERT [dbo].[Orders] OFF
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
