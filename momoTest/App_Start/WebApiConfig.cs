﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace momoTest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
               name: "Customers",
               routeTemplate: "customers",
               defaults: new { controller = "values" }
            );

            config.Routes.MapHttpRoute(
                name: "CustomersById",
                routeTemplate: "customers/{CustomerId}",
                defaults: new { controller = "values", CustomerId = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
                name: "GetCustomerOrderHistory",
                routeTemplate: "customers/{customer_id}/orders",
                defaults: new { controller = "values", customer_id = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
                name: "GetCustomerOrdersById",
                routeTemplate: "customers/{customerID}/orders/{orderId}",
                defaults: new { controller = "values", customerID = RouteParameter.Optional, orderid = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
               name: "CreateOrders",
               routeTemplate: "orders/create/{desc}/{amount}/{quantity}/{customerID}",
               defaults: new { controller = "values",
                   desc = RouteParameter.Optional,
                   amount = RouteParameter.Optional,
                   quantity = RouteParameter.Optional,
                   customerID = RouteParameter.Optional
               }
            );

            config.Routes.MapHttpRoute(
              name: "CreateCustomer",
              routeTemplate: "customers/create/{company}/{country}/{address}/{contactName}/{phone}",
              defaults: new
              {
                  controller = "values",
                  company = RouteParameter.Optional,
                  country = RouteParameter.Optional,
                  address = RouteParameter.Optional,
                  contactName = RouteParameter.Optional,
                  phone = RouteParameter.Optional
              }
           );
        }
    }
}
