﻿using momoTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace momoTest.Controllers
{
   
    public class ValuesController : ApiController
    {

        // GET customers  -- get all customers in the database
        public List<CustomerModel> GetAllCustomers()
        {
            momoDBContextDataContext momoContext = new momoDBContextDataContext();
            return momoContext.Customers.Select(c => new CustomerModel()
            {
                ContactName = c.ContactName,
                CompanyName = c.CompanyName,
                PhoneNumber = c.PhoneNumber
            }).ToList();
        }

        // GET customers/{CustomerID} -- get  full details from a specific customer
        public CustomerModel GetCustomerById(int CustomerId)
        {
            momoDBContextDataContext momoContext = new momoDBContextDataContext();
            return momoContext.Customers.Select(c => new CustomerModel()
            {
                CustomerId = c.CustomerID,
                CompanyName = c.CompanyName,
                Country = c.Country,
                Address = c.Address,
                ContactName = c.ContactName,
                PhoneNumber = c.PhoneNumber
            }).Where(m => m.CustomerId == CustomerId).FirstOrDefault();

        }

        // GET  customers/{customerid}/orders -- get all orders from a specific customer
        public List<OrderModel> GetCustomerOrderHistory(int customer_id)
        {
            var db = new momoDBContextDataContext();
            return (from c in db.Customers
                    join o in db.Orders
                    on c.CustomerID equals o.CustomerID
                    where c.CustomerID == customer_id
                    select new OrderModel
                    {
                        Description = o.Description,
                        Amount = o.Amount,
                    }).ToList();
        }

        // GET  customers/{customerid}/orders/{orderid}  -- Order detail by orderid and customerId
        public OrderModel GetCustomerOrdersById(int customerID, int orderid)
        {
            var db = new momoDBContextDataContext();
            OrderModel result = (from c in db.Customers
                                 join
                                 o in db.Orders on c.CustomerID equals o.CustomerID
                                 where c.CustomerID == customerID && o.OrderID == orderid
                                 select new OrderModel
                                 {
                                     OrderId = o.OrderID,
                                     Description = o.Description,
                                     Date = Convert.ToDateTime(o.Date),
                                     Amount = o.Amount,
                                     Quantity = o.Quantity,
                                     CustomerId = o.CustomerID
                                 }).FirstOrDefault();
            return result;

        }

   

        // POST orders/create/[param1][param2]...
        public string CreateOrders(string desc, double amount, int quantity,int customerID)
        {
            momoDBContextDataContext momoContext = new momoDBContextDataContext();
            // Create a new Order object.
            Order ord = new Order
            {
                Description = desc,
                Date = System.DateTime.Now,
                Amount = amount,
                Quantity = quantity,
                CustomerID = customerID
            };

            // Add the new object to the Orders collection.
            momoContext.Orders.InsertOnSubmit(ord);

            // Submit the change to the database.
            try
            {
                momoContext.SubmitChanges();
                return "Record added successfully";
            }
            catch (Exception e)
            {
                return "An error has occurred" + e;
            }
        }


        // POST customer/create/[param1][param2]...
        public string CreateCustomer(string company, string country, string address, string contactName, string phone)
        {
            momoDBContextDataContext momoContext = new momoDBContextDataContext();
            // Create a new Customer object.
            Customer cust = new Customer
            {
                CompanyName = company,
                Country = country,
                Address = address,
                ContactName = contactName,
                PhoneNumber = phone
            };

            // Add the new object to the Customers collection.
            momoContext.Customers.InsertOnSubmit(cust);

            // Submit the change to the database.
            try
            {
                momoContext.SubmitChanges();
                return "Record added successfully";
            }
            catch (Exception e)
            {
                return "An error has occurred" + e;
            }
        }

        public int suma(int num1, int num2)
        {
            return num1 + num2;
        }
    }
}
