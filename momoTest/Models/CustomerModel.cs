﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace momoTest.Models
{
    public class CustomerModel
    {
        [Key]
        public int CustomerId { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string ContactName { get; set; }
        public string TelephoneNumber { get; set; }
        public string PhoneNumber { get; set; }

    }
}