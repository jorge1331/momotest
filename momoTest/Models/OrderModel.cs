﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace momoTest.Models
{
    public class OrderModel
    {
        [Key]
        public int OrderId { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public System.Nullable<double> Amount { get; set; }
        public System.Nullable<int> Quantity { get; set; }
        public Customer Customer { get; set; }
        public System.Nullable<int> CustomerId { get; set; }
    }
}