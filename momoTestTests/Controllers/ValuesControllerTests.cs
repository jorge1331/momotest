﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using momoTest.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using momoTest.Models;

namespace momoTest.Controllers.Tests
{
    [TestClass()]
    public class ValuesControllerTests
    {
        //Global Arrange
        momoDBContextDataContext momoContext = new momoDBContextDataContext();
        ValuesController val = new ValuesController();

        [TestMethod()]
        public void GetAllCustomersTest()
        {
            //Arrange
            int i = 0;
            List<CustomerModel> expectedCustomer = momoContext.Customers.Select(c => new CustomerModel()
            {
                ContactName = c.ContactName,
                CompanyName = c.CompanyName,
                TelephoneNumber = c.PhoneNumber
            }).ToList();

            //Act
            List<CustomerModel> actualCustomer = val.GetAllCustomers.ToList();

            //Assert
            foreach (var c in actualCustomer)
            {

                Assert.AreEqual(c.CompanyName, expectedCustomer[i].CompanyName);
                Assert.AreEqual(c.ContactName, expectedCustomer[i].ContactName);
                Assert.AreEqual(c.TelephoneNumber, expectedCustomer[i].TelephoneNumber);
                i++;
            }
        }

        [TestMethod()]
        public void GetCustomerByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetCustomerOrderHistoryTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetCustomerOrdersByIdTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void CreateOrdersTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void CreateCustomerTest()
        {
            Assert.Fail();
        }

    }
}